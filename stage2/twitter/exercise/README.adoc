// Asciidoctor Source
// Nearsoft Ruby Stage 2, Task Interview
//
// Original authors:
// - Gerardo Galíndez
//
// Last updated: June 15, 2016 (Mexico City)
// Notes:
//   Compile with: $ asciidoctor README.adoc

= Twitter
[Stage 2: Ruby]
:toc:
:showtitle:

*TL;DR;* Write a Twitter client that interacts with the REST API and fetches the
home timeline, user timeline, the account's retweets and posts a tweet.

Your name is Andrés Sepúlveda. You are a Colombian hacker that is off its face.
After a gruesome struggle to survive in the third world, you decide that you
have had enough. Thus, you embark in a journey to rig the politic climate of
other third world countries, along with those of powerful countries because
you're looking for a challenge.

A Mexican party hires you in order to help the TV's favorite candidate win
by sowing distrust in the rest of the candidates, all of them are more fit to
take on the dilapidated nation's social tragedies, *BUT THIS WILL BRING THE BIG
MUNNIES!*

And so, you need to build a Twitter client in order start your false flag
campaign.

You evil, you.

== Tasks
Let's wreak havoc. You need to write a client that interacts with Twitter's
https://dev.twitter.com/rest/public[*REST API*]. You need to auth using
http://oauth.net/[OAuth]. Fortunately, you stumbled into a
https://dev.twitter.com/oauth/overview/single-user#ruby[_very clear example_] in
the docs.

It's like democracy already wants to lose!

TIP: You already have an account and it's keys setup. Get your hands dirty.

The acceptance requirements are:

. Authenticate to the API using OAuth
. Fetch the user's timeline (The ones you are following)
. Fetch the user's tweets (The ones from your account)
. Post a new tweet

Don't sweat it. Most of them are one-liners.

You *can*:

- Add any dependencies (except for Twitter gems)
- Use any oauth library
- Use any HTTP client

You *cannot*:

- Use a Twitter API wrapper (That's not h4x0r) like:
https://rubygems.org/gems/twitter[`twitter`],
https://rubygems.org/gems/omniauth-twitter[`omniauth-twitter`], or
https://rubygems.org/gems/twitter-stream[`twitter-stream`].

== It's dangerous to go alone!
_Take this:_

=== Pitfalls
- The reference oauth library's syntax is less than optimal, but it works and
  requires zero config. Don't sweat it if it looks ugly.
- Timeline response's are JSON and are arrayed hashes representing tweets. If
  you write `query_api` and `fetch_tweets` you can one-line every method.

=== Tips and Tricks
- Oauth's request method's signature is `#request(method, url, {options})`

=== Useful stuff
- https://dev.twitter.com/oauth/overview/single-user#ruby[OAuth into the API]
- https://dev.twitter.com/rest/public[Twitter's REST API]
