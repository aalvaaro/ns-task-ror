require_relative 'twitter_client'
require 'minitest/autorun'
require 'awesome_print'
require 'pry'

class TestTwitterClient < Minitest::Test
  def setup
    @twitter_client = TwitterClient.new
  end

  def test_home_timeline
    result = @twitter_client.home_timeline
    refute_empty result
  end

  def test_user_timeline
    result = @twitter_client.user_timeline
    assert_equal result.count, 20
    assert result.all? {|tweet| tweet.user_name == 'ns_rb_interview'}
  end

  def test_retweets
    result = @twitter_client.retweets
    assert result.all? {|tweet| tweet.user_name == 'ns_rb_interview'}
    assert result.any? {|tweet| tweet.text == 'Behind every great computer sits a skinny little geek.'}
  end

  def test_post
    text = "This is a test! #{Time.now}"
    result = @twitter_client.post(text)

    assert result
    assert @twitter_client.user_timeline.first.text, text
  end
end
