require_relative 'twitter_client'

describe TwitterClient do
  before do
    @twitter_client = TwitterClient.new
  end

  it 'fetches the home timeline' do
    result = @twitter_client.home_timeline
    expect(result).not_to be_empty
  end

  it "fetches it's own timeline" do
    result = @twitter_client.user_timeline
    expect(result.count).to eq 20
    expect(result.all? {|tweet| tweet.user_name == 'ns_rb_interview'}).to be true
  end

  it "fetches the account's retweets" do
    result = @twitter_client.retweets
    expect(result.all? {|tweet| tweet.user_name == 'ns_rb_interview'}).to be true
    expect(result.any? {|tweet| tweet.text == 'Behind every great computer sits a skinny little geek.'}).to be true
  end

  it 'post a tweet as the given account' do
    text = "This is a test! #{Time.now}"
    result = @twitter_client.post(text)

    expect(result).to be true
    expect(@twitter_client.user_timeline.first.text).to eq text
  end
end
