require_relative 'tweet'
require 'json'
require 'oauth'

# Twitter user: @ns_rb_interview
class TwitterClient
  CONSUMER_KEY = 'xgdqyJkRiKX1yJy5Y9eKNNzbe'
  CONSUMER_SECRET = 'sgZpa5bfua1OPJQeRXxh2PCbn4SGMD3FJsC9EsZLPgmWPGc4k1'
  OAUTH_KEY = '4094958314-Hgohqptu8x1X3ea8qo7kkcdLqSAOgVS5UZnHzKQ'
  OAUTH_SECRET = 'Nd8Navw4rMkMWNIdZMhN7mypODnxkdX0YtcqDDHiUa44z'

  BASE_URI = 'https://api.twitter.com/1.1'

  def initialize
    consumer = OAuth::Consumer.new(CONSUMER_KEY, CONSUMER_SECRET, {site: "https://api.twitter.com", scheme: :header})
    token_hash = {oauth_token: OAUTH_KEY, oauth_token_secret: OAUTH_SECRET}
    @client = OAuth::AccessToken.from_hash(consumer, token_hash)
  end

  # Query the API and return a response body
  def query_api(method, path, options = nil)
    JSON.parse(@client.request(method, "#{BASE_URI}#{path}", options).body)
  end

  # Return an array of Tweet objects from the given URL's response
  def fetch_tweets(url)
    response = query_api(:get, url)
    response.map{|tweet| Tweet.new(tweet['user']['screen_name'], tweet['text'])}
  end

  def home_timeline
    fetch_tweets('/statuses/home_timeline.json')
  end

  def user_timeline
    fetch_tweets('/statuses/user_timeline.json')
  end

  def retweets
    fetch_tweets('/statuses/retweets_of_me.json')
  end

  def post(status)
    response = query_api(:post, '/statuses/update.json', status: status)
    !response.nil?
  end
end
