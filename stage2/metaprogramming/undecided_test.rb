require 'digest'
require 'minitest'
require 'minitest/autorun'
require_relative 'undecided'

class UndecidedTest < Minitest::Test
  def setup
    @undecided = Undecided.new
  end

  def test_undefined_method
    random_method = random_string
    error = assert_raises { @undecided.public_send(random_method) }
    assert_equal NoMethodError, error.class
  end

  def test_ghost_method
    ghost_method = "ghost_#{random_string}"
    assert_equal 'Boo!', @undecided.public_send(ghost_method)
    refute_respond_to @undecided, ghost_method
  end

  def test_defined_method
    new_method = "def_#{random_string}"
    refute_respond_to @undecided, new_method, "Hard coding is bad!"
    assert_equal new_method.to_sym, @undecided.public_send(new_method)
    assert_respond_to @undecided, new_method
  end

  private
  def random_string(length = 5)
    Digest::SHA256.hexdigest(Time.now.to_s)[1..length]
  end
end
