# You don't need to change anything here to pass this test. This is just here
# for cleaner output for when you inevitably screw up the error chain.

module Cleanable
  def method_missing(meth, *args)
    raise NoMethodError, "'#{meth}' is undefined, infinite loop detected." if locked?(meth)
    lock_method(meth) { super }
  end

  private
  def lock_method(meth)
    @locked[meth] = true
    result = yield
    @locked[meth] = false
    result
  end

  def locked?(meth)
    @locked[meth]
  end
end

class Undecided
  # We could use prepend here, but we need to make this 1.9.3 compatible.
  def initialize
    @locked ||= {}
    singleton_class.class_eval { include Cleanable }
  end
end
