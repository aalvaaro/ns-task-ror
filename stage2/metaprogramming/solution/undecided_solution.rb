require_relative '../cleanable'

class Undecided
  def method_missing(meth, *args)
    case meth.to_s
    when /^ghost_/
      'Boo!'
    when /^def_/
      self.class.send :define_method, meth, ->{ meth }
      public_send(meth)
    else
      super
    end
  end
end
