require 'minitest'
require 'minitest/autorun'
require_relative 'game'
require_relative 'dsl'

class GameTest < Minitest::Test
  def test_move
    game = Game.play do |g|
      g ||= self
      g.move
    end
    assert_equal 0, game.distance, 'Should not move unless called within a light'
  end

  def test_redlight!
    game = Game.play do |g|
      g ||= self
      g.redlight! {}
    end
    assert_nil game.result, 'Should have not lost game if move was not called during a redlight!'

    game = Game.play do |g|
      g ||= self
      g.redlight! do
        g.move
      end
    end
    assert game.lost?, 'Should have lost game if move called during a redlight!'
    assert_equal 0, game.distance, 'Should not move when called during a redlight!'
  end

  def test_greenlight!
    game = Game.play do |g|
      g ||= self
      g.greenlight! do
        g.move
      end
    end
    assert_equal 10, game.distance, 'Should move if called during a greenlight!'

    game = Game.play do |g|
      g ||= self
      g.greenlight! do
        11.times { g.move }
      end
    end
    assert game.won?, 'Should win if moved to winning distance'
    assert_equal 100, game.distance, 'Should not move more after the game has finished'
  end

  def test_bonus
    game = Game.play do
      greenlight! do
        3.times { move }
      end

      redlight! {}

      greenlight! do
        2.times {move}
      end

      redlight! {}

      greenlight! do
        3.times {move}
      end

      greenlight! do
        2.times {move}
      end
    end
    assert game.won?, 'Should have won the game!'
  rescue NoMethodError
    flunk 'Not a true DSL yet :('
  end
end
