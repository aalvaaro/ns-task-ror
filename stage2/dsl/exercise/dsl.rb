class Game::DSL
  MOVE_DISTANCE = 10
  WIN_DISTANCE  = 100

  def initialize(game)
    @game = game
  end

  # TODO
  # Moving during a redlight will cause the game to end and lose
  def redlight!
  end

  # TODO
  # Moving during a greenlight will allow the player to move
  def greenlight!
  end

  # TODO
  # If called outside a #greenlight! or #redlight! nothing happens
  # Moves the player MOVE_DISTANCE
  # If MOVE_DISTANCE >= WIN_DISTANCE game ends and wins
  def move
  end
end
