class Game
  Finished = Class.new RuntimeError

  STATUS  = %i[standby playing finished]
  RESULTS = %i[won lost]

  attr_accessor :result, :distance

  def self.play(&block)
    new.play(&block)
  end

  def initialize
    @status   = :standby
    @distance = 0
    @dsl = Game::DSL.new(self)
  end

  def play(&block)
    catch :finish! do
      start!
      @dsl.instance_eval(&block)
      finish!
    end
    self
  end

  def start!
    @status = :playing
  end

  def finish!
    @status = :finished
    freeze
    throw :finish!
  end

  # Defines #result=
  # Ensures you don't typo out of the valid results
  def result=(value)
    raise "'#{value}' is not a valid result. (Check for Typos)" unless RESULTS.include? value
    @result = value
  end

  # Defines #standby? #playing? #finished?
  STATUS.each do |status|
    define_method("#{status}?") { @status == status }
  end

  # Defines #won? #lost?
  RESULTS.each do |result|
    define_method "#{result}?" do
      raise "##{__method__} called before game finished!" unless finished?
      @result == result
    end
  end
end
