require_relative 'game'
require_relative 'dsl'

describe Game do
  context '#move' do
    it 'should not move unless called within a light' do
      game = Game.play do |g|
        g ||= self
        g.move
      end
      expect(game.distance).to eq 0
    end
  end

  context '#redlight!' do
    it 'should do nothing if move was not called' do
      game = Game.play do |g|
        g ||= self
        g.redlight! {}
      end
      expect(game.result).to be_nil
    end

    it 'should finish and lose the game if move is called' do
      game = Game.play do |g|
        g ||= self
        g.redlight! do
          g.move
        end
      end
      expect(game.lost?).to be true
      expect(game.distance).to eq 0
    end
  end

  context '#greenlight!' do
    it 'should move if move is called' do
      game = Game.play do |g|
        g ||= self
        g.greenlight! do
          g.move
        end
      end
      expect(game.distance).to eq 10
    end

    it 'should finish and win the game if move is called enough times' do
      game = Game.play do |g|
        g ||= self
        g.greenlight! do
          11.times { g.move }
        end
      end

      expect(game.won?).to be true
      expect(game.distance).to eq 100
    end
  end

  context 'bonus' do
    it 'should be a true dsl' do
      begin
        game = Game.play do
          greenlight! do
            3.times { move }
          end

          redlight! {}

          greenlight! do
            2.times {move}
          end

          redlight! {}

          greenlight! do
            3.times {move}
          end

          greenlight! do
            2.times {move}
          end
        end
        expect(game.won?).to be true
      rescue NoMethodError
        fail
      end
    end
  end
end
