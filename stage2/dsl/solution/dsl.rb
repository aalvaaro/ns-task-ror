class Game::DSL
  MOVE_DISTANCE = 10
  WIN_DISTANCE  = 100

  def initialize(game)
    @game = game
  end

  def redlight!
    @light = :red
    yield
    @light = nil
  end

  def greenlight!
    @light = :green
    yield
    @light = nil
  end

  def move
    case @light
    when :green
      @game.distance += MOVE_DISTANCE
      if @game.distance >= WIN_DISTANCE
        @game.result = :won
        @game.finish!
      end
    when :red
      @game.result = :lost
      @game.finish!
    end
  end
end
