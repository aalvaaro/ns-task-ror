require 'rails_helper'

describe LinksController do

  describe 'GET #show' do
    it 'translates a link and redirects to its target' do
      link = create(:link)

      get :show, id: link.digest

      expect(response).to have_http_status(302)
      expect(response).to redirect_to(link.url)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'encodes and returns a digest' do
        post :create, link: attributes_for(:link)

        result = JSON.parse(response.body)

        expect(response).to have_http_status(201)
        expect(result).to have_key('digest')
        expect(result['digest']).to eq Link.last.digest
      end
    end

    context 'with invalid params' do
      it 'fails and returns an error' do
        post :create, link: {url: 'invalid-url'}

        result = JSON.parse(response.body)

        expect(response).to have_http_status(422)
        expect(result).to have_key('errors')
        expect(result['errors']).to have_key('invalid_url')
        expect(result['errors']['invalid_url']).to include('Invalid URL')
      end
    end
  end
end
