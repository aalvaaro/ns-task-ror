require 'rails_helper'

describe Link do
  before do
    @link = build(:link)
  end

  context 'when valid' do
    it 'stores the link' do
      expect(@link).to be_valid
      @link.save
      expect(@link.persisted?).to be true
    end
  end

  context 'when invalid' do
    it 'fails on missing params' do
      @link.url = ''

      expect(@link).to be_invalid
      @link.save
      expect(@link.persisted?).to be false
    end

    it 'fails on an invalid url' do
      @link.url = 'an-invalid-url'

      expect(@link).to be_invalid
      @link.save
      expect(@link.persisted?).to be false
      expect(@link.errors).to include(:invalid_url)
    end
  end
end
