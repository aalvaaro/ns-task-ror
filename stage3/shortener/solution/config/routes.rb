Rails.application.routes.draw do
  resources :links, only: [:show, :create]
  match '/:id' => 'links#show', via: [:get], as: :root
end
