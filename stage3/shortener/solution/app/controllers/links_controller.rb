class LinksController < ApplicationController
  # GET /links/1
  # GET /links/1.json
  def show
    link = Link.find_by(digest: params[:id])
    redirect_to link.url
  end

  # POST /links
  # POST /links.json
  def create
    @link = Link.new(link_params)

    if @link.save
      render json: {digest: @link.digest}, status: :created, location: @link
    else
      render json: {errors: @link.errors}, status: :unprocessable_entity
    end
  end

  private
    def link_params
      params.require(:link).permit(:url)
    end
end
