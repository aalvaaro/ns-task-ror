class Link < ActiveRecord::Base
  validates :url, :digest, presence: true
  validate :valid_url
  before_validation :digest_link

  def valid_url
    errors.add(:invalid_url, 'Invalid URL') unless URI.parse(url).kind_of?(URI::HTTP)
  end

  def digest_link
    self.digest ||= Digest::SHA1.hexdigest(url)[0,6]
  end
end
