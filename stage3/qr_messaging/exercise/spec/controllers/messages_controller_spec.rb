require 'rails_helper'

describe MessagesController do
  describe "GET #show" do
    it "assigns the requested message as @message" do
      message = create(:message)
      get :show, {:id => message.url}
    end
  end

  describe "GET #new" do
    it "assigns a new message as @message" do
      get :new, {}
    end
  end

  describe "GET #preview" do
    it "assigns the requested message and qr code" do
      message = create(:message)
      get :preview, {:id => message.url}
    end
  end

  describe "POST #create" do
    it "creates a new Message" do
      expect {
        post :create, {:message => attributes_for(:message)}
      }.to change(Message, :count).by(1)
    end
  end
end
