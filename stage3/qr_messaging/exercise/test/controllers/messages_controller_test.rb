require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  test '#show' do
    message = create(:message)
    get :show, :id => message.url
  end

  test '#new' do
    get :new, {}
  end

  test '#preview' do
    message = create(:message)
    get :preview, :id => message.url
  end

  test '#create' do
    assert_difference('Message.count') do
      post :create, message: attributes_for(:message)
    end
  end
end
