require "test_helper"

class MessageTest < ActiveSupport::TestCase
  test 'valid message is saved' do
    message = build(:message)
  end

  test 'invalid message is not saved' do
    message = build(:message).tap {|m| m.url = ''}
  end
end
