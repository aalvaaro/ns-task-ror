class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :preview]

  # GET /messages/:url
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/new/:url
  def preview
    url = "#{own_ip}:#{request.port}#{message_path(@message.url)}"
    @qr = RQRCode::QRCode.new(url, :level => :h)
  end

  # POST /messages
  def create
    @message = Message.new(message_params)
    @message.generate_url

    respond_to do |format|
      if @message.save
        format.html { redirect_to new_message_path, notice: "Message created. URL: #{@message.url}" }
      else
        format.html { render :new }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find_by(url: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:url, :text)
    end

    def own_ip
      Socket.ip_address_list.find { |ai| ai.ipv4? && !ai.ipv4_loopback? }.ip_address
    end
end
