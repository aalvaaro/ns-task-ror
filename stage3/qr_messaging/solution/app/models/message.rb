class Message < ActiveRecord::Base
  validates :url, :text, presence: true

  def generate_url
    self.url = SecureRandom.hex(28)
  end
end
