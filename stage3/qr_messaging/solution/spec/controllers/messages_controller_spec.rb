require 'rails_helper'

describe MessagesController do
  describe "GET #show" do
    it "assigns the requested message as @message" do
      message = create(:message)
      get :show, {:id => message.url}

      expect(response).to have_http_status(:ok)
      expect(assigns(:message)).to eq(message)
    end
  end

  describe "GET #new" do
    it "assigns a new message as @message" do
      get :new, {}
      expect(response).to have_http_status(:ok)
      expect(assigns(:message)).to be_a_new(Message)
    end
  end

  describe "GET #preview" do
    it "assigns the requested message and qr code" do
      message = create(:message)
      get :preview, {:id => message.url}

      expect(response).to have_http_status(:ok)
      expect(assigns(:message)).to eq(message)
      expect(assigns(:qr))
    end
  end

  describe "POST #create" do
    it "creates a new Message" do
      expect {
        post :create, {:message => attributes_for(:message)}
      }.to change(Message, :count).by(1)

      expect(response).to have_http_status(:found)
    end
  end
end
