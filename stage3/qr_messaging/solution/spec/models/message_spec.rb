require 'rails_helper'

describe Message do
  context 'when valid' do
    subject { build(:message) }
    it { is_expected.to be_valid }
  end

  context 'when invalid' do
    subject { build(:message).tap {|m| m.url = ''} }
    it { is_expected.to be_invalid }
  end
end
