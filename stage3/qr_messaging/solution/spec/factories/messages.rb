FactoryGirl.define do
  factory :message do
    text "A very secure message."

    after(:build) {|message| message.generate_url}
    before(:create) {|message| message.generate_url}
  end
end
