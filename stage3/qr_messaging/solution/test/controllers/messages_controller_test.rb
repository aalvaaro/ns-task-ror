require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  test '#show' do
    message = create(:message)
    get :show, :id => message.url

    assert_response :success
    assert assigns(:message)
  end

  test '#new' do
    get :new, {}

    assert_response :success
    assert assigns(:message)
  end

  test '#preview' do
    message = create(:message)
    get :preview, :id => message.url

    assert_response :success
    assert assigns(:message)
    assert assigns(:qr)
  end

  test '#create' do
    assert_difference('Message.count') do
      post :create, message: attributes_for(:message)
    end

    assert_response :found
  end
end
