# Track test coverage
require 'simplecov'
SimpleCov.start

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "database_cleaner"
require "minitest/rails"
require "minitest/unit"

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  # Ensure that factories are working correctly before the test run
  begin
    DatabaseCleaner.start
    FactoryGirl.lint
  ensure
    DatabaseCleaner.clean
  end

  before :each do
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
  end
end
