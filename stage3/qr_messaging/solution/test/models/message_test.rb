require "test_helper"

class MessageTest < ActiveSupport::TestCase
  test 'valid message is saved' do
    message = build(:message)

    assert message.valid?
    assert message.save
  end

  test 'invalid message is not saved' do
    message = build(:message).tap {|m| m.url = ''}

    refute message.valid?
    refute message.save
  end
end
