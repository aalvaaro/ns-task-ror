Rails.application.routes.draw do
  resources :messages, only: [:show, :new, :create] do
    member do
      get 'preview'
    end
  end

  root 'messages#new'
end
