require_relative "countries"
require "minitest/autorun"

class TestCountries < Minitest::Test
  def setup
    @countries = Countries.new
  end

  def test_north_america_filter
    skip
    result = @countries.in_north_america

    assert result.find {|country| country.name == 'Mexico'}
    refute result.find {|country| country.name == 'Japan'}
  end

  def test_asia_filter
    skip
    result = @countries.in_asia

    assert result.find {|country| country.name == 'Japan'}
    refute result.find {|country| country.name == 'Mexico'}
  end

  def test_territory_sort
    skip
    result = @countries.by_territory

    assert_equal result[0].name, 'Russia'
    assert_equal result[1].name, 'Antarctica'
    assert_equal result[2].name, 'Canada'
    assert_equal result[3].name, 'United States'
  end

  def test_smallest_country
    skip
    assert_equal @countries.smallest_country.name, 'Vatican'
  end

  def test_population_sort
    skip
    result = @countries.by_population

    assert_equal result[0].name, 'China'
    assert_equal result[1].name, 'India'
    assert_equal result[2].name, 'United States'
    assert_equal result[3].name, 'Indonesia'
  end

  def test_total_pop_asia
    skip
    assert_equal @countries.total_pop_in_asia, 4130318467
  end

  def test_world_pop
    skip
    assert_equal @countries.world_pop, 6871843631
  end
end
