# Countries file description
# 0: ISO
# 1: ISO3
# 3: fips
# 4: Country
# 5: Capital
# 6: Area(in sq km)
# 7: Population
# 8: Continent
# 9: tld
# 10: CurrencyCode
# 11: CurrencyName
# 12: Phone
# 13: Postal Code Format
# 14: Postal Code Regex
# 15: Languages
# 16: geonameid
# 17: neighbours
# 18: EquivalentFipsCode

class Countries
  attr_accessor :data
  Struct.new('Country', :name, :capital, :area, :population, :continent, :currency_name)

  def initialize
    @data = []
    File.open('countries.txt', 'r').each_line do |line|
      info = line.split("\t")
      country = Struct::Country.new(info[4], info[5], info[6].to_i, info[7].to_i, info[8], info[11])
      @data << country
    end
  end

  def in_north_america
  end

  def in_asia
  end

  def by_territory
  end

  def smallest_country
  end

  def by_population
  end

  def total_pop_in_asia
  end

  def world_pop
  end

  # Print: [<continent>]: <Country> (Capital city)
  def self.pretty_print(countries)
  end
end
