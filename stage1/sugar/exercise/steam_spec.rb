require_relative 'steam'

describe Steam do
  let(:steam) { Steam.new }

  context 'constructor' do
    it 'assigns the user sent via new' do
      steam = Steam.new(user: 'Ironman')
      expect(steam.user).to eq 'Ironman'
    end
  end

  context 'getters' do
    it 'responds to all the getters' do
      %w[user title games].each do |getter|
        expect(steam).to respond_to getter
        expect(steam.instance_variable_get("@#{getter}")).to eq steam.public_send(getter)
      end
    end
  end

  context 'setters' do
    it 'responds to all the setters' do
      %w[user title].each do |attr|
        setter = "#{attr}="
        expect(steam).to respond_to setter

        steam.public_send(setter, :new_value)
        expect(steam.instance_variable_get("@#{attr}")).to eq :new_value
      end
    end

    it 'doesnt define new setters' do
      expect(steam).to_not respond_to :game=
    end
  end

  context '#add_game' do
    it 'adds the game to the collection' do
      steam.add_game(:wolfenstein)
      expect(steam.games).to eq [:wolfenstein]
    end
  end

  context '#count_games' do
    it 'counts the current number of games correctly' do
      expect(steam.count_games).to eq 0
      (1..3).each do |i|
        steam.add_game(i.to_s)
        expect(steam.count_games).to eq i
      end
    end
  end

  context '#update_title' do
    it 'updates the title correctly' do
      expect(steam.title).to eq :newbee
      101.times {|i| steam.add_game(i) }
      steam.update_title
      expect(steam.title).to eq :master
    end
  end

  context '#to_s' do
    it 'returns the correct string' do
      expect(steam.to_s).to eq 'User: Anon. Title: newbee'
    end
  end
end
