class Steam
  # Declaring variables
  @user; @title; @games

  # Constructor method
  # TODO Find out why this isnt working
  def Steam(options = {})
    @user  = options[:user] || 'Anon'
    @title = :newbee
    @games = Array.new
  end

  # Getters and setters

  def user
    return @user
  end

  def user=(user)
    @user = user
  end

  def title
    return @title
  end

  def title=(title)
    @title = title
  end

  def games
    return @games
  end

  # Other functionality

  # Adds a game to the games list
  def add_game(game)
    games << game
  end

  # Counts the number of games
  def count_games
    count = 0
    for game in games
      count += 1 # Why isnt there a ++?!
    end
    return count
  end

  # Updates the user title based on the number of games he has.
  def update_title
    current_title = title
    if current_title = :master
      return
    end

    if count_games >= 100
      new_title = :master
    else
      new_title = :newbee
    end
    title = new_title
  end

  # Prints to_s
  def to_s
    return 'User: ' + user + '. Title: ' + title
  end
end
