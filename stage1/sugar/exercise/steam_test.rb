require 'minitest'
require 'minitest/autorun'
require_relative 'steam'

class SteamTest < Minitest::Test
  def setup
    @steam = Steam.new
  end

  def test_constructor
    @steam = Steam.new(user: 'Ironman')
    assert_equal 'Ironman', @steam.user, 'Should assign the user sent via new.'
  end

  def test_getters
    %w[user title games].each do |getter|
      assert_respond_to @steam, getter, "Should respond to #{getter}"
      assert_equal @steam.instance_variable_get("@#{getter}"), @steam.public_send(getter)
    end
  end

  def test_setters
    %w[user title].each do |attr|
      setter = "#{attr}="
      assert_respond_to @steam, setter, "Should respond to #{setter}"

      @steam.public_send(setter, :new_value)
      assert_equal :new_value, @steam.instance_variable_get("@#{attr}"), "#{setter} should assign the new value."
    end
    refute_respond_to @steam, :games=, "Don't add more methods! (:games=)"
  end

  def test_add_game
    @steam.add_game(:wolfenstein)
    assert_equal [:wolfenstein], @steam.games
  end

  def test_count_games
    assert_equal 0, @steam.count_games
    (1..3).each do |i|
      @steam.add_game(i.to_s)
      assert_equal i, @steam.count_games
    end
  end

  def test_update_title
    assert_equal :newbee, @steam.title
    101.times {|i| @steam.add_game(i) }
    @steam.update_title
    assert_equal :master, @steam.title, "Should be master after updating the title."
  end

  def test_to_s
    assert_equal 'User: Anon. Title: newbee', @steam.to_s
  end
end
