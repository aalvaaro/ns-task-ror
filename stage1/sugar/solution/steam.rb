class Steam
  # Ideally all 'return's must be removed.

  # The declarations should be removed.
  # Not only are they needless but they're in the wrong scope.
  # Bonus points if the interviewee says they're class instance variables.

  # Games should be a reader only
  attr_reader :games
  # These two need to accessors
  attr_accessor :user, :title

  # Should use initialize here, Array.new to [].
  # Extra points if the interviewee uses named arguments
  # def initialize(name: 'Anon')
  def initialize(options = {})
    @user  = options[:user] || 'Anon'
    @title = :newbee
    @games = []
  end

  # Should use @games
  def add_game(game)
    @games << game
  end

  # Count is the easiest way, he can also reduce or sum. That's overcomplicating tho.
  def count_games
    games.count
  end

  # The redundant code can be eliminated or left be.
  # This is fine as it's easy to read.
  # For the assignment should use @title= or self.title=
  def update_title
    @title = count_games >= 100 ? :master : :newbee
  end

  # Should be changed to interpolations.
  def to_s
    "User: #{user}. Title: #{title}"
  end
end
