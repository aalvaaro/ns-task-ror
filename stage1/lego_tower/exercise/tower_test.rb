require 'minitest'
require 'minitest/autorun'
require_relative 'tower'

class TowerTest < Minitest::Test
  def setup
    @tower = Tower.new
  end

  def test_initializer
    @tower = Tower.new {|l| l.add_lego :green }
    assert_equal 0, @tower.lego_bricks.size,  'Should not execute the block'
    assert_equal 1, @tower.actions.size, 'Should save the received block'

    @tower = Tower.new
    assert_equal 0, @tower.actions.size, 'Should save nothing if no block was received'
  end

  def test_add_action
    @tower.add_action { |l| l.add_lego :red }
    assert_equal 0, @tower.lego_bricks.size,  'Should not execute the block'
    assert_equal 1, @tower.actions.size, 'Should save the received block'

    @tower.add_action { |l| l.add_lego :blue }
    assert_equal 2, @tower.actions.size, 'Should not delete the received blocks if called multiple times'
  end

  def test_build
    @tower.add_action { |l| l.add_lego :yellow }
    @tower.add_action do |l|
      3.times { l.add_lego :orange }
    end
    @tower.build

    assert_equal 4, @tower.lego_bricks.size, 'Should execute the saved blocks'
    assert_equal 0, @tower.actions.size,     'Should clean up the received actions'
    assert_equal %i[yellow orange orange orange], @tower.lego_bricks.map(&:color),
      'Should execute the actions in the order received'
  end

  def test_step
    @tower.add_action { |l| l.add_lego :green }
    @tower.add_action { |l| l.step }
    @tower.add_action { |l| l.add_lego :blue }
    @tower.build

    assert_equal 0, @tower.lego_bricks.size, 'Should clear the LEGO bricks'
    assert_equal 1, @tower.actions.size,     'Should not discard the rest of the actions'

    @tower.build
    assert_equal 0, @tower.actions.size, 'Should not prevent build if called again'
  end
end
