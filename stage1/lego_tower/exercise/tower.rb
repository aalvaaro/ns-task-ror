class Tower
  attr_reader :actions, :lego_bricks

  # TODO
  def initialize
    @actions      = []
    @lego_bricks  = []
    @stepped      = false
    yield(self)
  end

  # TODO
  # Adds an action to do with your lego tower!
  def add_action
  end

  # Adds a lego to you tower!
  def add_lego(type = nil)
    @lego_bricks << Lego.new(type)
  end

  # TODO
  # Runs all the actions that were saved
  def build
  end

  # TODO
  # Steps on your current lego tower
  # Stops build from executing other actions
  def step
  end
end

class Lego
  COLORS = %i[red yellow orange green blue]
  attr_reader :color

  def initialize(color = nil)
    @color =
      case color.to_sym
      when *COLORS
        color
      else
        COLORS.shuffle.first
      end
  end
end
