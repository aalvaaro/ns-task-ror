require_relative 'tower'

describe Tower do
  let(:tower){ Tower.new }

  context '#initialize' do
    it 'saves the received block, rather than execute it' do
      tower = Tower.new {|l| l.add_lego :green }
      expect(tower.lego_bricks.size).to eq 0
      expect(tower.actions.size).to eq 1
    end

    it 'saves nothing if no block was received' do
      tower = Tower.new
      expect(tower.actions.size).to eq 0
    end
  end

  context '#add_action' do
    it 'saves any number of received blocks correctly' do
      tower.add_action { |l| l.add_lego :red }
      expect(tower.lego_bricks.size).to eq 0
      expect(tower.actions.size).to eq 1

      tower.add_action { |l| l.add_lego :blue }
      expect(tower.actions.size).to eq 2
    end
  end

  context '#build' do
    it 'executes all the saved actions in the order received and clears them' do
      tower.add_action { |l| l.add_lego :yellow }
      tower.add_action do |l|
        3.times { l.add_lego :orange }
      end
      tower.build

      expect(tower.lego_bricks.size).to eq 4
      expect(tower.actions.size).to eq 0
      expect(tower.lego_bricks.map(&:color)).to eq %i[yellow orange orange orange]
    end
  end

  context '#step' do
    it 'clears the LEGO bricks pile and halts build until its called again' do
      tower.add_action { |l| l.add_lego :green }
      tower.add_action { |l| l.step }
      tower.add_action { |l| l.add_lego :blue }
      tower.build

      expect(tower.lego_bricks.size).to eq 0
      expect(tower.actions.size).to eq 1

      tower.build
      expect(tower.actions.size).to eq 0
    end
  end
end
