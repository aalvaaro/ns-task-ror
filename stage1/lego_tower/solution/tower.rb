class Tower
  attr_reader :actions, :lego_bricks

  def initialize(&block)
    @actions      = []
    @lego_bricks  = []
    @stepped      = false
    add_action(&block) if block_given?
  end

  # Adds an action to do with your lego tower!
  def add_action(&block)
    @actions << block
  end

  # Adds a lego to you tower!
  def add_lego(type = nil)
    @lego_bricks << Lego.new(type)
  end

  # Runs all the actions that were saved
  def build
    @actions.shift.call(self) until @actions.empty? || @stepped
    @stepped = false
  end

  # Steps on your current lego tower
  # Stops build from executing other actions
  def step
    @lego_bricks.clear
    @stepped = true
  end
end

class Lego
  COLORS = %i[red yellow orange green blue]
  attr_reader :color

  def initialize(color = nil)
    @color =
      case color.to_sym
      when *COLORS
        color
      else
        COLORS.shuffle.first
      end
  end
end
