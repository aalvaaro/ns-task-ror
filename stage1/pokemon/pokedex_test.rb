require 'minitest'
require 'minitest/autorun'
require_relative 'pokedex'

class PokedexTest < Minitest::Test
  def test_charizard_vs_venosaur
    result = Pokedex::Charizard.attack Pokedex::Venosaur
    assert_equal result, SUPER_EFFECTIVE
  end

  def test_venosaur_blastoise
    result = Pokedex::Venosaur.attack Pokedex::Blastoise
    assert_equal result, SUPER_EFFECTIVE
  end

  def test_charizard_vs_blastoise
    result = Pokedex::Charizard.attack Pokedex::Blastoise
    assert_equal result, NOT_VERY_EFFECTIVE
  end

  def test_venosaur_vs_charizard
    result = Pokedex::Venosaur.attack Pokedex::Charizard
    assert_equal result, NOT_VERY_EFFECTIVE
  end

  def test_charizard_is_fire
    assert Pokedex::Charizard.singleton_class.include? Fire
  end

  def test_charizard_can_attack
    assert Pokedex::Charizard.respond_to?(:attack)
    assert_equal Pokedex::Charizard.method(:attack).owner, Fire
  end
end
