require 'pry'
require_relative 'pokemon_types'

module Pokedex
  class Charizard
    extend Fire
  end

  class Venosaur
    extend Grass
  end

  class Blastoise
    extend Water
  end
end
