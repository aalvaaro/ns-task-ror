require_relative 'pokedex'

describe Pokedex do
  context 'as Charizard' do
    it 'is super effective against Grass' do
      result = Pokedex::Charizard.attack Pokedex::Venosaur
      expect(result).to eq SUPER_EFFECTIVE
    end

    it 'is not effective against Water' do
      result = Pokedex::Charizard.attack Pokedex::Blastoise
      expect(result).to eq NOT_VERY_EFFECTIVE
    end

    it 'is of type Fire' do
      expect(Pokedex::Charizard.singleton_class.include?(Fire)).to eq true
    end

    it 'can attack as a Fire type' do
      expect(Pokedex::Charizard).to respond_to(:attack)
      expect(Pokedex::Charizard.method(:attack).owner).to eq Fire
    end
  end

  context 'as Venosaur' do
    it 'is super effective against Water' do
      result = Pokedex::Venosaur.attack Pokedex::Blastoise
      expect(result).to eq SUPER_EFFECTIVE
    end

    it 'is not effective against Fire' do
      result = Pokedex::Venosaur.attack Pokedex::Charizard
      expect(result).to eq NOT_VERY_EFFECTIVE
    end
  end
end
