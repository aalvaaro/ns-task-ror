require 'pry'

SUPER_EFFECTIVE = "It's super effective!"
NOT_VERY_EFFECTIVE = "It's not very effective..."

module Fire
  def attack other_pokemon
    other_pokemon.singleton_class.include?(Grass) ? SUPER_EFFECTIVE : NOT_VERY_EFFECTIVE
  end
end

module Grass
  def attack other_pokemon
    other_pokemon.singleton_class.include?(Water) ? SUPER_EFFECTIVE : NOT_VERY_EFFECTIVE
  end
end

module Water
  def attack other_pokemon
    other_pokemon.singleton_class.include?(Fire) ? SUPER_EFFECTIVE : NOT_VERY_EFFECTIVE
  end
end
